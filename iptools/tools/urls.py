from django.urls import path, register_converter

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('subnet-info/<ip_address>/<net_mask>', views.subnet_info, name='subnet_info'),
    path('ip-info/<ip_address>/<net_mask>', views.ip_info, name='ip_info'),
    path('ip-info/', views.ip_info, name='ip_info'),
    path('subnet-check/', views.subnet_check, name='subnet_check'),
    path('ip-check/', views.ip_check, name='ip_check')
]
