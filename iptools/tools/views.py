from netaddr import IPNetwork, valid_ipv4, valid_ipv6

from django.shortcuts import HttpResponse
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .forms import IPInputForm
from .utils import get_ip_info


def index(request):
    # Use request.META['REMOTE_ADDR'] for default

    # We actually create a network instead of an address to encapsulate the
    # supernet/subnet context around the address available through the netaddr API

    # IPs without a prefix should maybe default to /32 or /128?

    form = IPInputForm()

    ip_address = IPNetwork('1.1.1.1/24')
    available_hosts = ip_address.size - 2
    return render(request, 'tools/index.html', {
        'form': form, 'available_hosts': available_hosts,
        'ip_address': ip_address
    })


def subnet_check(request):
    print('In subnet_check')
    if request.method == 'POST':
        print('In POST', request.POST)
        form = IPInputForm(request.POST)
        if form.is_valid():
            ip_address = IPNetwork(form.cleaned_data['ip_address'])
            print(request.POST)

            return redirect(f'/tools/subnet-info/{str(ip_address.ip)}/{ip_address.prefixlen}')
    # if request.method == 'GET':
    #     print('GET received on ip_check')


def ip_check(request):
    print('In ip_check')
    if request.method == 'POST':
        print('In POST', request.POST)
        form = IPInputForm(request.POST)
        if form.is_valid():
            ip_address = IPNetwork(form.cleaned_data['ip_address'])
            print(request.POST)

            return redirect(f'/tools/ip-info/{str(ip_address.ip)}/{ip_address.prefixlen}')
    # if request.method == 'GET':
    #     print('GET received on ip_check')


def subnet_info(request, ip_address=None, net_mask=None):
    # TODO: If only an IP is submitted, perhaps redirect to IP info page?
    # Also the inverse?
    if request.method == 'POST':
        form = IPInputForm(request.POST)
        if form.is_valid():
            ip_address = IPNetwork(form.cleaned_data['ip_address'])
            print(request.POST)

            return redirect(f'/tools/sub-info/{str(ip_address.ip)}/{ip_address.prefixlen}')
    # TODO: Handle exception from invalid IP

    if request.method == 'GET':
        if ip_address:
            ip_address = IPNetwork(f'{ip_address}/{net_mask}')
            form = IPInputForm()
            return render(request, 'tools/subnet_info.html', context={'form': form, 'ip_address': ip_address})
        else:
            form = IPInputForm()
            return render(request, 'tools/subnet_info.html', context={'form': form})


def ip_info(request, ip_address=None, net_mask=None):
    if request.method == 'POST':
        form = IPInputForm(request.POST)
        if form.is_valid():
            ip_address = IPNetwork(form.cleaned_data['ip_address'])
            print(request.POST)

            return redirect(f'/tools/ip-info/{str(ip_address.ip)}/{ip_address.prefixlen}')
    # TODO: Handle exception from invalid IP

    if request.method == 'GET':
        if ip_address:
            ip_address = IPNetwork(f'{ip_address}/{net_mask}')
            ip_details = get_ip_info(str(ip_address.ip))
            form = IPInputForm()
            return render(request, 'tools/ip_info.html', context={
                'form': form, 'ip_address': ip_address, 'ip_details': ip_details
            })
        else:
            form = IPInputForm()
            return render(request, 'tools/ip_info.html', context={'form': form})
