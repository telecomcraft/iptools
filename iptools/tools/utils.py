import json

import ipdata
import ipinfo
import netaddr
import requests

from iptools.settings import SECRETS

handler = ipinfo.getHandler(SECRETS['IP_INFO_KEY'])
ipdata.api_key = SECRETS['IP_DATA_KEY']


def get_ip_info(ip_address):
    # TODO: Check if IP is public before making API call
    ip_address = netaddr.IPNetwork(ip_address)
    ip_details = handler.getDetails(str(ip_address.ip))
    return ip_details


def get_ip_data(ip_address):
    # TODO: Check if IP is public before making API call
    ip_address = netaddr.IPNetwork(ip_address)
    ip_details = ipdata.lookup(str(ip_address.ip))
    return ip_details
