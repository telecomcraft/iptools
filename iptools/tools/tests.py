from re import match
from netaddr import IPNetwork

from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

from . import views, forms, utils


class UtilsTest(TestCase):

    def test_ip_info(self):
        ip_address = '1.1.1.1'
        details = utils.get_ip_info(ip_address)
        print(details)

    def test_ip_data(self):
        ip_address = '1.1.1.1'
        details = utils.get_ip_data(ip_address)
        print(details)


class HomePageTest(TestCase):

    def test_home_page_returns_correct_html(self):
        # TODO: Update to actual content
        request = HttpRequest()
        response = views.index(request)
        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<!doctype html>'))
        self.assertIn('<title>IP Tools</title>', html)
        self.assertTrue(html.endswith('</html>'))


class ToolPageTest(TestCase):

    def test_index_page_exists(self):
        # TODO: Test for main tool page
        response = self.client.get('/tools/')
        self.assertTemplateUsed(response, 'tools/index.html')
        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<!doctype html>'))
        self.assertIn('<title>IP Tools</title>', html)
        self.assertTrue(html.endswith('</html>'))

    def test_ip_in_view(self):
        request = HttpRequest()
        ip = IPNetwork('192.168.1.8/24')
        print(ip)
        return True

    def test_ip_input_form(self):
        form = forms.IPInputForm()
        self.fail('Finish the test!')
