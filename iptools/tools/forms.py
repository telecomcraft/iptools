from django import forms


class IPInputForm(forms.Form):
    """Main input field for IP addresses."""
    ip_address = forms.CharField(max_length=100, label='IP Address')


class MacAddressInfoForm(forms.Form):
    ip = forms.CharField(max_length=100, label='IP Address')
    # netmask = forms.CharField(max_length=4, label='Netmask')


class DomainInfoForm(forms.Form):
    pass
