from django.db import models



class EUIRegistry(models.Model):
    """The EUIs used as MAC Addresses in devices in each network must be tracked to ensure they all remain unique. This
    global table maintains EUI utilization within each network. RFC 7042 Documentation range will be used, but that
    restricts each network to only 255 interfaces."""

    eui = models.CharField(max_length=50)

class Network(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()


class Device(models.Model):
    network = models.ForeignKey(Network, on_delete=models.RESTRICT)
    name = models.CharField(blank=True, null=True, max_length=200)
    hostname = models.CharField(blank=True, null=True, max_length=200)
    device_type = 'Device'
    description = models.TextField()
    is_enabled = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def enable(self):
        self.is_enabled = True
        print(f'{self.device_type} {self.name} is now enabled.')

    def disable(self):
        self.is_enabled = False
        print(f'{self.device_type} {self.name} is now disabled.')


class ForwardingDevice(Device):

    interface_label = models.CharField(default='eth', max_length=10)
    interface_start = models.IntegerField(default=0)
    interface_end = models.IntegerField(default=23)

    class Meta:
        abstract = True


class FIBEntry(models.Model):
    # device = models.ForeignKey(ForwardingDevice, on_delete=models.DO_NOTHING)
    mac_address = models.CharField(max_length=200)
    route = models.CharField(max_length=200)
    interface = models.CharField(max_length=200)


class RIBEntry(models.Model):
    # device = models.ForeignKey(ForwardingDevice, on_delete=models.DO_NOTHING)
    mac_address = models.CharField(max_length=200)
    route = models.CharField(max_length=200)
    interface = models.CharField(max_length=200)


class Switch(ForwardingDevice):

    device_type = 'Switch'

    def _init_interfaces(self):
        interfaces = {}
        for i in range(self.interface_start, self.interface_end + 1):
            interface = SwitchInterface.objects.create(device=self, number=i, label=self.interface_label)
            interfaces[interface.number] = interface
            print(f'Adding switch interface {i}')
        return interfaces


class SwitchInterface(models.Model):  # TODO: Should this have a base class?
    device = models.ForeignKey(Switch, on_delete=models.CASCADE, related_name='interfaces')
    name = models.CharField(max_length=200)
    number = models.IntegerField()
    vlan = models.IntegerField(null=False, blank=False, default=1)
    label = models.CharField(default='eth', max_length=10)

    def __str__(self):
        return f'{self.label}{self.number}'

    def __repr__(self):
        return f'{self.label}{self.number}'


class Router(ForwardingDevice):

    device_type = 'Router'

    def add_interface(self):
        print('Adding router interface')

    def add_route(self):
        pass

    def drop_route(self):
        pass

    def show_interfaces(self):
        pass

    def show_routes(self):
        pass


class RouterInterface(models.Model):
    device = models.ForeignKey(Router, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    vlan = models.IntegerField(null=False, blank=False, default=1)
