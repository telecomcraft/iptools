from netaddr import EUI, valid_mac
from django.test import TestCase

from .models import Network, Switch, Router
from .utils import generate_eui48_block


class UtilsTests(TestCase):

    def test_eui48_seed(self):
        eui = generate_eui48_block()
        print(eui)
        self.assertTrue(valid_mac(str(eui)))


class NetworkTests(TestCase):

    def test_new_network(self):
        network = Network.objects.create(name='Test Network')
        self.assertEqual(network.name, 'Test Network')


class SwitchTests(TestCase):

    def setUp(self):
        pass

    def test_new_switch(self):
        network = Network.objects.create(name='Test Network')
        switch = Switch.objects.create(name='Test Switch', network=network)
        self.assertEqual(switch.name, 'Test Switch')

        switch.enable()
        self.assertTrue(switch.is_enabled)
        switch.disable()
        self.assertFalse(switch.is_enabled)

        # Let's try a custom interface label
        switch.interface_label = 'eno'
        self.assertEqual(switch.interface_label, 'eno')
        interfaces = switch._init_interfaces()
        self.assertEqual(interfaces[0].number, 0)
        self.assertEqual(len(interfaces), 24)
        self.assertEqual(switch.interfaces.count(), 24)


class RouterTests(TestCase):

    def test_new_switch(self):
        network = Network.objects.create(name='Test Network')
        router = Router.objects.create(name='Test Router', network=network)
        self.assertEqual(router.name, 'Test Router')

        router.enable()
        self.assertTrue(router.is_enabled)
        router.disable()
        self.assertFalse(router.is_enabled)

        router.add_interface()
