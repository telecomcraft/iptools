from random import randint, choice
from netaddr import EUI


def generate_eui48_block(block_type='unicast'):
    """Generate usable EUI seed for network from locally-administered ranges.
    Based on https://en.wikipedia.org/wiki/MAC_address#Universal_addresses_that_are_administered_locally"""

    unicast_blocks = ('2', '6', 'A', 'E')
    multicast_blocks = ('3', '7', 'B', 'F')

    if block_type == 'unicast':
        block = (choice(unicast_blocks))
    elif block_type == 'multicast_groups':
        block = (choice(multicast_blocks))

    digits = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F')
    # We construct the OUI using a random hex digit and one of the
    # second least-significant digits from the local-admin blocks above
    return EUI(f'{choice(digits)}{block}-BB-CC-00-00-00')


if __name__ == '__main__':
    eui_start = generate_eui48_block()
    print(eui_start)
    print(eui_start.bits())
