from django.core.exceptions import ValidationError
from django.db import models
from netaddr import AddrFormatError, IPNetwork


class BaseIPField(models.Field):
    """Base class for the IP-based fields. Modeled after NetBox's similar
    fields, which also utilize netaddr."""

    def python_type(self):
        return IPNetwork

    def from_db_value(self, value, expression, connection):
        return self.to_python(value)

    def to_python(self, value):
        if not value:
            return value
        try:
            # Always return a netaddr.IPNetwork object. (netaddr.IPAddress does not provide a mask.)
            return IPNetwork(value)
        except AddrFormatError:
            raise ValidationError("Invalid IP address format: {}".format(value))
        except (TypeError, ValueError) as e:
            raise ValidationError(e)

    def get_prep_value(self, value):
        if not value:
            return None
        if isinstance(value, list):
            return [str(self.to_python(v)) for v in value]
        return str(self.to_python(value))


class IPAddressField(BaseIPField):
    """
    IP address (host address and mask)
    """
    description = "SQLite varchar-based IP address field"

    def db_type(self, connection):
        return 'varchar'


class IPNetworkField(BaseIPField):
    """
    IP prefix (network and mask)
    """

    # TODO: migrate to PostgreSQL
    description = "SQLite varchar-based IP network field"
    # default_validators = [validators.prefix_validator]

    def db_type(self, connection):
        return 'varchar'
