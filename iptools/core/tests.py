from django.test import TestCase
from django.urls import resolve

from core import views
from .models import CachedIPAddress, CachedIPNetwork


def test_root_url_resolves_to_home_page_view(self):
    found = resolve('/')
    self.assertEqual(found.func, views.home_page)


class ModelsTest(TestCase):

    def test_ip_address_model(self):
        ip = CachedIPAddress.objects.create(ip='1.1.1.1/32')
        self.assertEqual(ip.ip, '1.1.1.1/32')
        key = ip.save()
        saved_ip = CachedIPAddress.objects.get(id=1)
        self.assertEqual(str(saved_ip.ip), '1.1.1.1/32')
        print(ip.id)
