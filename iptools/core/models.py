from django.db import models


from .fields import IPAddressField, IPNetworkField


class CachedMACAddress(models.Model):
    pass


class CachedIPAddress(models.Model):

    ip = IPAddressField()

    def __str__(self):
        return str(self.ip)

    class Meta:
        verbose_name = 'Cached IP Address'
        verbose_name_plural = 'Cached IP Addresses'


class CachedIPNetwork(models.Model):
    """
    The IPNetwork model represents an aggregate of all the most common
    fields used on the platform as well as cached values from data providers.
    """

    ip = IPNetworkField()

    def __str__(self):
        print(self.ip)
        return str(self.ip)

    class Meta:
        verbose_name = 'Cached IP Network'
        verbose_name_plural = 'Cached IP Networks'
