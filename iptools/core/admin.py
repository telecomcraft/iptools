from django.contrib import admin


from .models import CachedIPAddress, CachedIPNetwork


class CachedIPAddressAdmin(admin.ModelAdmin):
    pass


class CachedIPNetworkAdmin(admin.ModelAdmin):
    pass


admin.site.register(CachedIPAddress, CachedIPAddressAdmin)
admin.site.register(CachedIPNetwork, CachedIPNetworkAdmin)
